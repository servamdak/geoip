CREATE DATABASE geoip_db;
USE geoip_db;
CREATE TABLE `ip2location_db`(
                                  `ip_from` INT(10) UNSIGNED,
                                  `ip_to` INT(10) UNSIGNED,
                                  `country_code` CHAR(2),
                                  `country_name` VARCHAR(64),
                                  `region_name` VARCHAR(128),
                                  `city_name` VARCHAR(128),
                                  `latitude` DOUBLE,
                                  `longitude` DOUBLE,
--                                   INDEX `idx_ip_from` (`ip_from`),
--                                   INDEX `idx_ip_to` (`ip_to`),
--                                   INDEX `idx_ip_from_to` (`ip_from`, `ip_to`),
                                  PRIMARY KEY (`ip_from`, `ip_to`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_bin;