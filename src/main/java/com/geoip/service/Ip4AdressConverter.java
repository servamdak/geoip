package com.geoip.service;

public interface Ip4AdressConverter {
    long ip4ToNumberConvert(String ip4);
    String numberToIp4Convert(long ip4Number);
}
