package com.geoip.service;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class Ip4AdressConverterImpl implements Ip4AdressConverter {

    private static final long BIT_COEFFICIENT = 256;


    public long ip4ToNumberConvert(String ip4) {

        long[] arrayOfIp4Parts = Arrays.stream(ip4.split("\\."))
                .mapToLong(Long::parseLong)
                .toArray();

        long result = 0;

        for (int i = arrayOfIp4Parts.length - 1; i >= 0; i--) {
            result += (arrayOfIp4Parts[i] % BIT_COEFFICIENT)
                    * Math.pow(BIT_COEFFICIENT, ((arrayOfIp4Parts.length - 1) - i));
        }

        return result;

    }


    public String numberToIp4Convert(long ip4Number) {

        String result = Stream.of(ip4Number)
                .flatMap(i -> Stream.iterate(3, x -> x >= 0, x -> x - 1)
                        .map(x -> ip4PartCalculate(ip4Number, x)))
                .map(Object::toString)
                .collect(Collectors.joining("."));

        return result;
    }


    private static int ip4PartCalculate(long ip4Number, int partNumber) {
        return (int) (ip4Number / Math.pow(BIT_COEFFICIENT, partNumber) % BIT_COEFFICIENT);
    }
}
