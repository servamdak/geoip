package com.geoip.service;

import com.geoip.models.IPId;
import com.geoip.models.IPLocation;

public interface IpDataService {
    IPLocation getIp4Data(long id);
}
