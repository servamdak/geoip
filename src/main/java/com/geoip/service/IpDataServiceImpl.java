package com.geoip.service;

import com.geoip.models.IPId;
import com.geoip.models.IPLocation;
import com.geoip.repositories.IpDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class IpDataServiceImpl implements IpDataService {

    @Autowired
    private IpDataRepository dataRepository;

    @Override
    public IPLocation getIp4Data(long id) {
        return dataRepository.findIPLocationBetweenIpFromAndIpTo(id);
    }
}
