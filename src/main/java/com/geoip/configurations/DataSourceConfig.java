package com.geoip.configurations;

import com.geoip.models.IPId;
import com.geoip.models.IPLocation;
import com.geoip.service.IpDataService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("com.geoip.repositories")
public class DataSourceConfig {

    @Value("${spring.datasource.driver}")
    private static String DATASOURCE_DRIVER;

    @Value("${spring.datasource.url}")
    private static String DATASOURCE_URL;

    @Value("${spring.datasource.username}")
    private static String DATASOURCE_USERNAME;

    @Value("${spring.datasource.password}")
    private static String DATASOURCE_PASSWORD;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private static String HIBERNATE_DDL_AUTO;

    @Value("${spring.jpa.show-sql}")
    private static String HIBERNATE_SHOW_SQL;

    @Value("${spring.jpa.properties.hibernate.dialect}")
    private static String HIBERNATE_DIALECT;


    @Resource
    private Environment environment;


    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty(DATASOURCE_DRIVER
        ));
        dataSource.setUrl(environment.getRequiredProperty(DATASOURCE_URL));
        dataSource.setUsername(environment.getRequiredProperty(DATASOURCE_USERNAME));
        dataSource.setPassword(environment.getRequiredProperty(DATASOURCE_PASSWORD));
        return dataSource;
    }


    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty(HIBERNATE_DDL_AUTO, environment.getRequiredProperty(HIBERNATE_DDL_AUTO));
        properties.setProperty(HIBERNATE_SHOW_SQL, environment.getRequiredProperty(HIBERNATE_SHOW_SQL));
        properties.setProperty(HIBERNATE_DIALECT, environment.getRequiredProperty(HIBERNATE_DIALECT));
        return properties;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setPackagesToScan("com.geoip.models"); //maybe a mistake
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        factoryBean.setJpaProperties(getHibernateProperties());
        return factoryBean;
    }

    @Bean
    public JpaTransactionManager jpaTransactionManager(EntityManagerFactory factory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(factory);
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslationPostProcessor(){
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
