package com.geoip.controllers;

import com.geoip.models.IPLocation;
import com.geoip.service.Ip4AdressConverter;
import com.geoip.service.IpDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController("/geoip")
public class Ip4Controller {

    @Autowired
    private IpDataService dataService;

    @Autowired
    private Ip4AdressConverter ip4AdressConverter;

    @GetMapping("/{ip4Adress}")
    public IPLocation getRequestedIp4Data(@PathVariable String ip4Adress) {
        return dataService.getIp4Data(ip4AdressConverter.ip4ToNumberConvert(ip4Adress));
    }

}
