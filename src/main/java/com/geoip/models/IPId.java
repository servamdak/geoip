package com.geoip.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
public class IPId implements Serializable {
    @Column(name = "ip_from")
    private Long ipFrom;

    @Column(name = "ip_to")
    private Long ipTo;
}
