package com.geoip.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "ip2location_db5")
public class IPLocation {

    @EmbeddedId
    private IPId id;

    @Column (name = "country_code")
    private String countryCode;

    @Column (name = "country_name")
    private String countryName;

    @Column (name = "region_name")
    private String regionName;

    @Column (name = "city_name")
    private String cityName;

    @Column (name = "latitude")
    private Double latitude;

    @Column (name = "longitude")
    private Double longitude;
}
