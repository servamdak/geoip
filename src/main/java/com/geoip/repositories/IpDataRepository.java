package com.geoip.repositories;

import com.geoip.models.IPId;
import com.geoip.models.IPLocation;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IpDataRepository extends JpaRepository<IPLocation, Long> {
    IPLocation findIPLocationBetweenIpFromAndIpTo(long id);
}
